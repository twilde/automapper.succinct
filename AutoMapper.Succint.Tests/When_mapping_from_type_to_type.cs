﻿namespace AutoMapper.Succint.Tests
{
   using System;
   using NUnit.Framework;
   using Succinct;
   using TestTypes;

   [ TestFixture ]
   public class When_mapping_from_type_to_type
   {
      [ SetUp ]
      public void Set_up_test_context()
      {
         Mapper.Reset();
         Mapper.CreateMap<Source, SubPart>();

         Mapper.CreateMap<Source, Destination>()
               .For( destination => destination.This ).Use( source => source.That )
               .For( destination => destination.Token ).Generate( _ => Guid.NewGuid() )
               .For( destination => destination.Date ).Generate( source =>
                                                                 {
                                                                    DateTime date;
                                                                    return DateTime.TryParse( source.Date, out date ) ? date : (DateTime?) null;
                                                                 } )
               .For( destination => destination.SubPart ).Use( source => source )
               .For( destination => destination.Number ).When( source => source.Number.IsOdd() ).Use( source => source.Number * 2 )
               .Ignore( destination => destination.Id );

         Mapper.AssertConfigurationIsValid();

         Source = new Source { That = "Value", Id = long.MaxValue, Date = "01/2014", Number = 3 };
      }

      private static Source Source { get; set; }

      [ Test ]
      public void Should_map_from_source_to_destination()
      {
         var mapped = Mapper.Map<Destination>( Source );

         mapped.This.Should( Be.EqualTo( "Value" ) );
      }

      [ Test ]
      public void Should_ignore_specified_member()
      {
         var mapped = Mapper.Map<Destination>( Source );

         mapped.Id.Should( Be.EqualTo( default( long ) ) );
      }

      [ Test ]
      public void Should_generate_a_value_during_mapping()
      {
         var one = Mapper.Map<Destination>( Source );
         var two = Mapper.Map<Destination>( Source );

         one.Token.Should( Be.Not.EqualTo( Guid.Empty ) );
         two.Token.Should( Be.Not.EqualTo( Guid.Empty ) );
         one.Token.Should( Be.Not.EqualTo( two.Token ) );
      }

      [ Test ]
      public void Should_be_able_to_map_a_source_type_into_a_destination_type_member()
      {
         var destination = Mapper.Map<Destination>( Source );

         destination.SubPart.Name.Should( Be.EqualTo( Source.Name ) );
         destination.SubPart.Description.Should( Be.EqualTo( Source.Description ) );
      }

      [ Test ]
      public void Should_be_able_to_use_lambdas_with_a_body_in_generate()
      {
         var destination = Mapper.Map<Destination>( Source );

         destination.Date.Should( Be.EqualTo( new DateTime( 2014, 1, 1 ) ) );

         var source = Source;
         source.Date = string.Empty;

         destination = Mapper.Map<Destination>( source );
         destination.Date.Should( Be.Null );
      }

      [ Test ]
      public void Should_be_able_to_map_conditionally()
      {
         var destination = Mapper.Map<Destination>( Source );

         destination.Number.Should( Be.EqualTo( Source.Number * 2 ) );
      }
   }
}
