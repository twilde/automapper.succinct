﻿namespace AutoMapper.Succint.Tests.TestTypes
{
   using System;

   public class Destination
   {
      public long Id { get; set; }
      public string This { get; set; }
      public Guid Token { get; set; }
      public SubPart SubPart { get; set; }
      public DateTime? Date { get; set; }
      public int Number { get; set; }
   }
}
