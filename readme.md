# AutoMapper.Succinct #
AutoMapper.Succinct is a set of extension methods which make the AutoMapper syntax a little more succinct.

The Succinct extensions add three extensions at the moment:

 - **Ignore** - takes one or more expressions to specify which members of the destination type should be ignored
 - **For ... Use** - Takes two expressions to specify which destination member is mapped from which source member
 - **For ... Generate** - Resolves the destination value using the function provided.

The default syntax can get a bit verbose:

	Mapper.CreateMap<IUserAccountDetails, ApplicationUser>()
          .ForMember( user => user.UserName, configuration => configuration.MapFrom( details => details.Email ) )
          .ForMember( user => user.EmailConfirmed, configuration => configuration.Ignore() )
		  .ForMember( user => user.Token, configuration => configuration.ResolveUsing( _ => Guid.NewGuid() );

With these extensions, it is a bit clearer:

    Mapper.CreateMap<IUserAccountDetails, ApplicationUser>()
          .For( user => user.UserName ).Use( details => details.Email )
		  .For( user => user.Token ).Generate( _ => Guid.NewGuid() )
          .Ignore( user => user.EmailConfirmed );

The succinct extensions can be mixed with the default syntax as needed:

	Mapper.CreateMap<IUserAccountDetails, ApplicationUser>()
	      .ForMember( user => user.Roles, configuration => configuration.DoNotUseDestinationValue() )
          .Ignore( user => user.Id )
          .ForMember( user => user.PasswordHash, configuration => configuration.Ignore() )
          .For( user => user.UserName ).Use( details => details.Email );
