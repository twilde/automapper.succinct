﻿namespace AutoMapper.Succinct
{
   using System;
   using System.Linq.Expressions;

   public static class AutoMapperExtensions
   {
      /// <summary>
      ///    Ignore this member for configuration validation and skip during mapping
      /// </summary>
      /// <remark>Shorthand for calling the ForMember ... Ignore combination.</remark>
      public static IMappingExpression<TSource, TDestination> Ignore<TSource, TDestination>( this IMappingExpression<TSource, TDestination> mapping,
                                                                                             Expression<Func<TDestination, object>> member,
                                                                                             params Expression<Func<TDestination, object>>[] members )
      {
         if( member == null )
            throw new ArgumentNullException( "member", "member must be specified" );

         mapping.ForMember( member, o => o.Ignore() );

         foreach( var m in members )
            mapping.ForMember( m, o => o.Ignore() );

         return mapping;
      }

      /// <summary>
      ///    Customize configuration for individual member
      /// </summary>
      /// <remarks>Shorthand for calling ForMember</remarks>
      public static IMappingConfiguration<TSource, TDestination> For<TSource, TDestination>( this IMappingExpression<TSource, TDestination> mapping,
                                                                                             Expression<Func<TDestination, object>> member )
      {
         if( member == null )
            throw new ArgumentNullException( "member", "member must be specified" );

         return new MappingConfiguration<TSource, TDestination>( mapping, member );
      }

      public interface IMappingConfiguration<TSource, TDestination>
      {
         /// <summary>
         ///    Specify the source member to map from. Can only reference a member on the TSource type This method can be used in
         ///    mapping to LINQ query projections, while ResolveUsing cannot. Any null reference exceptions in this expression will
         ///    be ignored (similar to flattening behavior)
         /// </summary>
         /// <remark>Shorthand for calling the ForMember ... MapFrom combination.</remark>
         IMappingExpression<TSource, TDestination> Use<TMember>( Expression<Func<TSource, TMember>> source );

         /// <summary>
         ///    Resolve destination member using a custom value resolver callback. Used instead of MapFrom when not simply
         ///    redirecting a source member This method cannot be used in conjunction with LINQ query projection
         /// </summary>
         /// <param name="generator">Callback function to resolve against source type</param>
         IMappingExpression<TSource, TDestination> Generate<TMember>( Func<TSource, TMember> generator );

         IMappingConfiguration<TSource, TDestination> When( Func<TSource, bool> member );
      }

      private sealed class MappingConfiguration<TSource, TDestination>: IMappingConfiguration<TSource, TDestination>
      {
         private readonly IMappingExpression<TSource, TDestination> mapping;
         private readonly Expression<Func<TDestination, object>> member;
         private Func<TSource, bool> condition;

         public MappingConfiguration( IMappingExpression<TSource, TDestination> mapping, Expression<Func<TDestination, object>> member )
         {
            this.mapping = mapping;
            this.member = member;
         }

         public IMappingExpression<TSource, TDestination> Use<TMember>( Expression<Func<TSource, TMember>> source )
         {
            if( condition != null )
               mapping.ForMember( member, o =>
                                          {
                                             o.Condition( condition );
                                             o.MapFrom( source );
                                          } );

            return mapping.ForMember( member, o => o.MapFrom( source ) );
         }

         public IMappingExpression<TSource, TDestination> Generate<TMember>( Func<TSource, TMember> generator )
         {
            return mapping.ForMember( member, o => o.ResolveUsing( x => generator( x ) ) );
         }

         public IMappingConfiguration<TSource, TDestination> When( Func<TSource, bool> condition )
         {
            this.condition = condition;
            return this;
         }
      }
   }
}
